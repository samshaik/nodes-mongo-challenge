# Book Management API

This is a simple Node.js RESTful API for managing books. It allows users to perform CRUD operations (Create, Read, Update, Delete) on book records. The API uses MongoDB for data storage.

## API Endpoints

- `POST /api/books`: Create a new book.
- `GET /api/books`: Retrieve a list of all books.
- `GET /api/books/:id`: Retrieve details of a specific book by its ID.
- `PUT /api/books/:id`: Update a book's details.
- `DELETE /api/books/:id`: Delete a book.

## Getting Started

Follow these steps to set up and run the application locally.

### Prerequisites

1. Node.js: Make sure you have Node.js installed on your system

2. MongoDB: Ensure that MongoDB is installed and running on your system. You can install MongoDB locally or use a cloud-based solution like MongoDB Atlas.

### Installation

1. Clone the repository to your local machine:

   ```bash
   git clone

   ```

2. cd my-book-api

3. npm install

4. make .env file

   ```bash
   port = 3000
   MONGOURL = mongodb://localhost/book


   ```

5. node app.js

## API Usage

1. To create a new book, send a POST request to http://localhost:3000/api/books with the book details in the request body.
2. To retrieve a list of all books, send a GET request to http://localhost:3000/api/books.
3. To retrieve details of a specific book by its ID, send a GET request to http://localhost:3000/api/books/:id, replacing :id with the book's actual ID.
4. To update a book's details, send a PUT request to http://localhost:3000/api/books/:id, replacing :id with the book's actual ID, and provide the updated data in the request body.
5. To delete a book, send a DELETE request to http://localhost:3000/api/books/:id, replacing :id with the book's actual ID.

## Deployment on Render

- It’s easy to deploy a Web Service on Render. By Linking GitHub or GitLab repository and click Create Web Service.
- Render automatically builds and deploys your service every time you push to your repository.
- Runtime: Node
- Build Command: npm install
- Start Command: node app.js
- Added env variables like mongourl, port.

## You can directly use the below api's to test:

1. To create a new book, send a POST request to https://nodejs-mongo-4tmr.onrender.com/api/books with the book details in the request body.
2. To retrieve a list of all books, send a GET request to https://nodejs-mongo-4tmr.onrender.com/api/books.
3. To retrieve details of a specific book by its ID, send a GET request to https://nodejs-mongo-4tmr.onrender.com/api/books/:id, replacing :id with the book's actual ID.
4. To update a book's details, send a PUT request to https://nodejs-mongo-4tmr.onrender.com/api/books/:id, replacing :id with the book's actual ID, and provide the updated data in the request body.
5. To delete a book, send a DELETE request to https://nodejs-mongo-4tmr.onrender.com/api/books/:id, replacing :id with the book's actual ID.
