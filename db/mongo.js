const mongoose = require("mongoose");
require("dotenv").config();

function connectToMongoDB() {
  mongoose.connect(process.env.MONGOURL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  const db = mongoose.connection;
  db.on("error", (error) => {
    console.log(error);
  });
  db.once("open", () => {
    console.log("Connected to mongo DB");
  });
}

module.exports = connectToMongoDB;
