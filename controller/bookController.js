const express = require("express");
const router = express.Router();
const Book = require("../model/bookSchema");
router.post("/", async (req, res) => {
  const { title, author, summary } = req.body;
  const book = new Book({
    title,
    author,
    summary,
  });
  try {
    const newBook = await book.save();
    res.status(201).send(newBook);
  } catch (err) {
    res.status(400).send({ error: err.message });
  }
});

router.get("/", async (req, res) => {
  try {
    const books = await Book.find();
    res.status(200).send({
      status: "success",
      count : books.length,
      books,
    });
  } catch (err) {
    res.status(500).send({ error: err.message });
  }
});

router.get("/:id", async (req, res) => {
  const id = req.params.id;
  try {
    const book = await Book.findById(id);
    if (!book) {
      res.status(404).send({ error: `Book not found for the given Id: ${id}` });
      return;
    }
    res.status(200).send(book);
  } catch (err) {
    res.status(500).send({ error: err.message });
  }
});

router.put("/:id", async (req, res) => {
  const id = req.params.id;
  try {
    const book = await Book.findOneAndUpdate({ _id: id }, req.body);
    if (!book) {
      res.status(404).send({ error: `Book not found for the given Id: ${id}` });
    } else {
      res.status(200).send({
        status: "success",
        message: "Book Updated Successfully.",
      });
    }
  } catch (err) {
    res.status(500).send({ error: err.message });
  }
});

router.delete("/:id", async (req, res) => {
  const id = req.params.id;
  try {
    const book = await Book.findByIdAndDelete(id);
    if (!book) {
      res.status(404).send({ error: `Book not found for the given Id: ${id}` });
    } else {
      res
        .status(200)
        .send({ status: "success", message: "Book Deleted Successfully." });
    }
  } catch (err) {
    res.status(500).send({ error: err.message });
  }
});
module.exports = router;
