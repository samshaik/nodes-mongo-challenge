const handleErrors = (err, req, res, next) => {
  if (err.name === "NotFoundError") {
    res.status(404).send({ status: "error", message: "Not Found" });
    return "";
  }
};

module.exports = handleErrors;