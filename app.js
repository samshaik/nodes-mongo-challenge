const express = require("express");
const app = express();
const createError = require("http-errors");
const apiRouter = require("./routes/api");
const handleErrors = require("./middleware.js/handleErrors");
const connectToMongoDB = require("./db/mongo");
require("dotenv").config();

app.use(express.json());
const port = +process.env.PORT;

connectToMongoDB();

app.use("/api", apiRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(handleErrors);
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
