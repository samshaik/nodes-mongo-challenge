const express = require("express");
const app = express();
const bookController = require("../controller/bookController");

app.use("/books",bookController);

module.exports = app;
